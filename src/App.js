import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Demo from "./pages/Demo";
import LandingPage from "./pages/LandingPage";
import Register from "./pages/Register";

function App() {
  return (
    // <div className="App">
    //   <LandingPage />
    // </div>
    <Router>
      <div className="App">
        <Route exact path="/" component={LandingPage} />
        <Route path="/register" component={Register} />
        <Route path="/demo" component={Demo} />
      </div>
    </Router>
  );
}

export default App;
