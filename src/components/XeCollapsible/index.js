import React, { useState, useRef } from "react";
import PropTypes from "prop-types";
import "./xecollapsible.scss";

const XeCollapsible = ({ labelIcon, label, icon, children }) => {
  const [isOpen, setIsOpen] = useState(false);

  const parentRef = useRef();

  const handleToggle = () => {
    setIsOpen(!isOpen);
  };
  return (
    <div className={`collapsible ${isOpen ? "expanded" : ""}`}>
      <button className={`toggle`} onClick={() => handleToggle()}>
        <div className={`toggle-left-section`}>
          <span className={`toggle-left-section-label-icon`}>{labelIcon}</span>
          <span className={`toggle-left-section-label`}>{label}</span>
        </div>
        <div className={`toggle-right-section`}>
          <img src={icon} alt="drop down arrow" />
        </div>
      </button>
      <div
        className="content-parent"
        ref={parentRef}
        style={
          isOpen
            ? {
                height: parentRef.current.scrollHeight + "px",
              }
            : {
                height: "0px",
              }
        }
      >
        <div className="content">{children}</div>
      </div>
    </div>
  );
};

XeCollapsible.propTypes = {
  label: PropTypes.string,
  icon: PropTypes.string,
  children: PropTypes.any,
};

XeCollapsible.defaultProps = {
  label: "",
  icon: "",
  children: null,
};

export default XeCollapsible;
