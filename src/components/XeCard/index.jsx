import React from "react";
import PropTypes from "prop-types";
import "./xecard.scss";

const XeCard = ({ extraClass, children }) => {
  return <div className={`xc-landing-cards ${extraClass}`}>{children}</div>;
};

XeCard.propTypes = {
  extraClass: PropTypes.string,
  children: PropTypes.node,
};

XeCard.defaultProps = {
  extraClass: "",
  children: null,
};

export default XeCard;
