import React from "react";
import PropTypes from "prop-types";
import "./xebadge.scss";

const XeBadge = ({ icon, label, infoText }) => {
  return (
    <>
      <div class="badges-part">
        <div class="badges-content">
          <div class="badges-img">
            <img src={icon} alt={`logo`} />
          </div>
          <div class="badges-number">
            <h1>{label}</h1>
          </div>
          <div class="badges-text">{infoText}</div>
        </div>
      </div>
    </>
  );
};

XeBadge.propTypes = {};

export default XeBadge;
