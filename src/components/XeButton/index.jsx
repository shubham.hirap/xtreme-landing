import React from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import "./index.scss";
import ChevronRightBlack from "../../assets/icons/chevron_right_black_24dp.svg";
import ChevronRightWhite from "../../assets/icons/chevron_right_white_24dp.svg";

const XeButton = ({ label, onClick, hasColor, disabled }) => {
  return (
    <div className="xc-button-container">
      <button
        className={`xc-button  ${hasColor ? "has-bg" : ""} ${
          disabled ? "disabled" : ""
        }`}
        onClick={onClick}
      >
        <div className="xc-button-wrapper">
          <div className="xc-button-text">{label}</div>
          <div className="xc-button-icon-container">
            <div className="xc-button-icon">
              {hasColor ? (
                <img src={ChevronRightWhite} alt="chevron_right_white" />
              ) : (
                <img src={ChevronRightBlack} alt="chevron_right_black_24dp" />
              )}
            </div>
          </div>
        </div>
      </button>
    </div>
  );
};

XeButton.propTypes = {
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  className: PropTypes.string,
  hasColor: PropTypes.bool,
  disabled: PropTypes.bool,
};

XeButton.defaultProps = {
  label: "",
  onClick: _.noop,
  className: "",
  hasColor: false,
  disabled: false,
};

export default XeButton;
