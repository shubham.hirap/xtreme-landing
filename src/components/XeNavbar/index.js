import React from "react";
import { useHistory } from "react-router-dom";
import { Link } from "react-scroll";
import XeButton from "../XeButton";
import navLogo from "../../assets/images/Logo.png";
import DropDownArrow from "../../assets/icons/icon-dropdownarrow-black.svg";
import "./index.scss";

const XeNavbar = (props) => {
  const history = useHistory();
  return (
    <div className={`xe-navbar-container`}>
      <div className={`xe-navbar-wrapper`}>
        <div className="xe-navbar">
          <div className="brand-logo">
            <div className="brand-logo-container">
              <img className="res-img" src={navLogo} alt="Brnad logo" />
            </div>
          </div>
          <div className="nav-menu">
            <div className="nav-menu-item">
              <a className="nav-menu-item-text" href="#applications">
                <Link
                  activeClass={`active`}
                  to={`applications`}
                  spy={true}
                  smooth={true}
                  duration={800}
                >
                  APPLICATIONS
                </Link>
              </a>
              <div className="nav-menu-item-dropdown-icon">
                <img
                  className="img-res"
                  src={DropDownArrow}
                  alt="icon-dropdownarrow"
                />
              </div>
            </div>
            <a className="nav-menu-item" href="#pricing">
              <Link
                activeClass={`active`}
                to={`pricing`}
                spy={true}
                smooth={true}
                duration={800}
              >
                PRICING
              </Link>
            </a>
            <a className="nav-menu-item" href="#faq">
              <Link
                activeClass={`active`}
                to={`faq`}
                spy={true}
                smooth={true}
                duration={800}
              >
                FAQS
              </Link>
            </a>
            <a className="nav-menu-item active" href="">
              <Link
                activeClass={`active`}
                to={``}
                spy={true}
                smooth={true}
                duration={800}
              >
                LOGIN
              </Link>
            </a>
            <div className="nav-menu-item">
              <XeButton
                hasColor={false}
                label={`REGISTER`}
                onClick={() => history.push("/register")}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

XeNavbar.propTypes = {};

export default XeNavbar;
