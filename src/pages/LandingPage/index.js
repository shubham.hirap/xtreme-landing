import React from "react";
import XeNavbar from "../../components/XeNavbar";
import LandingPageFAQ from "../LandingPageFAQ";
import LandingPageFifthSection from "../LandingPageFifthSection";
import LandingPageFirstSection from "../LandingPageFirstSection";
import LandingPageFooter from "../LandingPageFooter";
import LandingPageFourthSection from "../LandingPageFourthSection";
import LandingPageSecondSection from "../LandingPageSecondSection";
import LandingPageSeventhSection from "../LandingPageSeventhSection";
import LandingPageSixthSection from "../LandingPageSixthSection";
import LandingPageThirdSection from "../LandingPageThirdSection";
import "./landingPage.scss";

const LandingPage = () => {
  return (
    <div className="xc-conteiner">
      <div className="xc-wrapper">
        <XeNavbar />
        <LandingPageFirstSection />
        <LandingPageSecondSection />
        <LandingPageThirdSection />
        <LandingPageFourthSection />
        <LandingPageFifthSection />
        <LandingPageSixthSection />
        <LandingPageSeventhSection />
        <LandingPageFAQ />
        <LandingPageFooter />
      </div>
    </div>
  );
};

export default LandingPage;
