import React from "react";
import Register from "../../assets/assets/landing/register.svg";
import Marketplace from "../../assets/assets/landing/marketplace.svg";
import Earn from "../../assets/assets/landing/earn.svg";
import Ipad from "../../assets/assets/landing/ipad.png";

import "./landingPageFourthSection.scss";

const LandingPageFourthSection = () => {
  return (
    <>
      <div className="xc-landing-forth-section">
        <div className="xc-landing-forth-section-container">
          <div className="xc-landing-forth-section-wrapper">
            <div className="aline-item-space-btwn">
              <div className="xc-landing-forth-left-section">
                <h1>START IN 3 SIMPLE STEPS</h1>
                <div className="display-inline">
                  <img src={Register} alt="" />
                  <h2>Register Your Brands</h2>
                </div>
                <div className="display-inline">
                  <img src={Marketplace} alt="" />
                  <h2>Select Marketplaces and Applications</h2>
                </div>
                <div className="display-inline">
                  <img src={Earn} alt="" />
                  <h2>You Earn and AI Work For You</h2>
                </div>
              </div>
              <div className="xc-landing-forth-right-section">
                <div className="xc-landing-right-section-ipad-image-container">
                  <img
                    className="res-img"
                    src={Ipad}
                    alt="marketplace view in ipad"
                  />
                </div>
              </div>
            </div>
            <div className="display-item-center-forth-section">
              <h1>
                Extreme Ecommerce is a suite of powerful applications that helps
                you to manage your multi channel <br /> business and grow your
                ecommerce reach from a single intuitive interface.
              </h1>
              <div>
                <h2>FBA profit & Loss Analysis Application</h2>
                <h2>Repricer Application</h2>
                <h2>Feedback Application</h2> <br />
              </div>
              <div>
                <h2>Reimbursement Application</h2>
                <h2>Order Management Application</h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default LandingPageFourthSection;
