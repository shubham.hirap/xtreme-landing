import React from "react";
import Envelope from "../../assets/assets/landing/envelope-regular.svg";
import FB from "../../assets/assets/landing/facebook.svg";
import IG from "../../assets/assets/landing/instagram.svg";
import Twitter from "../../assets/assets/landing/twitter.svg";
import Linkedin from "../../assets/assets/landing/linkedin-2.svg";
import YT from "../../assets/assets/landing/youtube.svg";
import ChatBox from "../../assets/assets/landing/icon-chatbox.svg";

import "./landingPageFooter.scss";

const LandingPageFooter = () => {
  return (
    <>
      <div class="xc-landing-footer-section">
        <div class="xc-landing-footer-section-container">
          <div class="xc-landing-footer-section-wrapper">
            <div class="aline-item-space-btwn">
              <div class="pannel-column footer-content">
                <h1>Applications</h1>
                <h2>Data One</h2>
                <h2>Buy Box</h2>
                <h2>Repay++</h2>
                <h2>Fivestar</h2>
                <h2>Order AI</h2>
              </div>
              <div class="pannel-column footer-content">
                <h1>Pricing</h1>
                <h2>Standard Plan</h2>
                <h2>Enterprise Plan</h2>
                <h2>Unlimited Plan</h2>
                <h2>Free Plan</h2>
              </div>
              <div class="pannel-column footer-content">
                <h1>FAQS</h1>
                <h2>Features</h2>
                <h2>Pricing</h2>
                <h2>Support</h2>
                <h2>Integrations</h2>
                <h2>System Requirements</h2>
              </div>
              <div class="pannel-column footer-content">
                <h1>Account</h1>
                <h2>Login</h2>
                <h2>Register</h2>
                <h2>Forgot Password</h2>
                <h3>
                  <img src={Envelope} alt="" />
                  Sales@xtremeecomerce.com
                </h3>
              </div>
            </div>
            <div class="aline-item-space-btwn">
              <div class="horizontal-footer-menu">
                <div class="footer-other-content">
                  <h1>Home</h1>
                  <h1>Contact Us</h1>
                  <h1>Security</h1>
                  <h1>Terms of Service</h1>
                  <h1>Privacy Policy</h1>
                  <h1>Cookie Policy</h1>
                  <h1>Abuse Policy</h1>
                </div>
                <div class="footer-social-meadia">
                  <img src={FB} alt="facebook logo" />
                  <img src={IG} alt="instagram logo" />
                  <img src={Twitter} alt="twitter logo" />
                  <img src={Linkedin} alt="linkedin logo" />
                  <img src={YT} alt="youtube logo" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="xc-chatbox-icon-section">
        <div class="xc-chatbox-icon">
          <img src={ChatBox} alt="chatbox logo" />
        </div>
      </div>
      <div class="xc-landing-footer-copyright">
        <div class="xc-landing-footer-copyright-container">
          <div class="xc-landing-footer-copyright-wrapper">
            <span>2021 Xtreme Ecommerce Pvt.Ltd. All Rights Reserved</span>
          </div>
        </div>
      </div>
    </>
  );
};

export default LandingPageFooter;
