import React from "react";
import DropDownArrow from "../../assets/assets/landing/icon-dropdownarrow-black.svg";
import XeCollapsible from "../../components/XeCollapsible";

import "./landingPageFaq.scss";

const LandingPageFAQ = () => {
  return (
    <>
      <div id="faq" class="xc-landing-eight-section">
        <div class="xc-landing-eight-section-container">
          <div class="xc-landing-eight-section-wrapper">
            <div class="aline-item-space-btwn">
              <h1>FAQS</h1>
              <span>
                Show all FAQs
                <img src={DropDownArrow} alt="" />
              </span>
            </div>
            <hr />
            <XeCollapsible
              label={`How can I request a product walkthrough?`}
              icon={DropDownArrow}
            ></XeCollapsible>
            <hr />
            <XeCollapsible
              label={`How to on board the brand?`}
              icon={DropDownArrow}
            ></XeCollapsible>
            <hr />
            <XeCollapsible
              label={`Is Registration paid?`}
              icon={DropDownArrow}
            ></XeCollapsible>
            <hr />
            <XeCollapsible
              label={`Free trials and refunds`}
              icon={DropDownArrow}
            ></XeCollapsible>
            <hr />
          </div>
        </div>
      </div>
    </>
  );
};

export default LandingPageFAQ;
