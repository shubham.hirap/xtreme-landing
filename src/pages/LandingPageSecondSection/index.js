import React from "react";
import USP1 from "../../assets/assets/landing/USP-1.png";
import BitMap2 from "../../assets/assets/landing/Bitmap (2).png";
import BitMap1 from "../../assets/assets/landing/Bitmap (1).png";
import BitMap from "../../assets/assets/landing/Bitmap.png";
import "./landingPageSecondSection.scss";

const LandingPageSecondSection = () => {
  return (
    <>
      <div className="xc-landing-second-section">
        <div className="xc-landing-second-section-container">
          <div className="xc-landing-second-section-wrapper">
            <div className="display-item-center">
              <span>TOP FEATURES</span>
            </div>
            <div className="aline-item-space-btwn ">
              <div className="xc-landing-left-section pannel-column ">
                <div className="xc-landing-column-item">
                  <div className="xc-landing-column-image-container">
                    <img className="res-img" src={USP1} alt="usp 1" />
                  </div>
                  <div>
                    Personalize communication with your customers to increase
                    trust in your brand.
                  </div>
                  <h3>
                    Our AI driven feedback application helps send requests to
                    the potential happy customers to increase good customer
                    review ratings. Also, it sends requests to remove unhealthy
                    reviews. This helps in increasing the trust and health of
                    the account on marketplaces.
                  </h3>
                </div>
                <div className="xc-landing-column-item">
                  <div className="xc-landing-column-image-container">
                    <img className="res-img" src={BitMap2} alt="bitmap" />
                  </div>

                  <div>
                    Retarget potential customers and increase chance of buybox
                  </div>
                  <h3>
                    Our proprietary repricing algorithms will increase your
                    sales transaction through competitive pricing and predict
                    when to discount, what to discount by analyzing various
                    dimensions of your account.
                  </h3>
                </div>
              </div>
              <div className="xc-landing-right-section pannel-column second-section">
                <div className="xc-landing-column-item">
                  <div className="xc-landing-column-image-container">
                    <img className="res-img" src={BitMap1} alt="bitmap" />
                  </div>
                  <div>
                    Manage multiple brands, marketplaces and applications, get
                    actionable recommendations and insights in one single
                    platform.
                  </div>
                  <h3>
                    Our platform will help to see a single view of all your
                    brands, onboarded marketplaces insights and let you work
                    with multiple applications under one single platform. There
                    is no need for you to try out other platforms or do
                    integrations at multiple platforms and then get the
                    insights. Let AI work for you.
                  </h3>
                </div>
                <div className="xc-landing-column-item">
                  <div className="xc-landing-column-image-container">
                    <img className="res-img" src={BitMap} alt="bitmap" />
                  </div>
                  <div>
                    Manage multiple brands, marketplaces and applications, get
                    actionable recommendations and insights in one single
                    platform.
                  </div>
                  <h3>
                    Our platform will help to see a single view of all your
                    brands, onboarded marketplaces insights and let you work
                    with multiple applications under one single platform. There
                    is no need for you to try out other platforms or do
                    integrations at multiple platforms and then get the
                    insights. Let AI work for you.
                  </h3>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default LandingPageSecondSection;
