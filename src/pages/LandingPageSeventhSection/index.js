import React from "react";
import { useHistory } from "react-router-dom";
import XeButton from "../../components/XeButton";
import "./landingPageSeventhSection.scss";

const LandingPageSeventhSection = () => {
  const history = useHistory();

  return (
    <>
      <div id="#faq" className="xc-landing-seven-section">
        <div className="xc-landing-seven-section-container">
          <div className="xc-landing-seven-section-wrapper">
            <span>LET’S CONNECT AND WE WILL HELP YOU GROW YOUR BUSINESS</span>
            <div className="xc-landing-seven-section-btn">
              <div className="aline-item-inline">
                <XeButton
                  className="seven-section-btn"
                  hasColor={true}
                  label={"REGISTER"}
                  onClick={() => history.push("/register")}
                />
                <XeButton
                  className="seven-section-btn"
                  hasColor={false}
                  label={`REQUEST A DEMO`}
                  onClick={() => history.push("/demo")}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default LandingPageSeventhSection;
