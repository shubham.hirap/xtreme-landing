import React from "react";
import RightArrow from "../../assets/assets/landing/rightarrow.png";
import DataOne from "../../assets/assets/landing/DataOne.png";
import BuyBox from "../../assets/assets/landing/BuyBox.png";
import Repay from "../../assets/assets/landing/Repay.png";
import FiveStar from "../../assets/assets/landing/FiveStar.png";
import OrderAI from "../../assets/assets/landing/OrderAi.png";

import "./landingPageThirdSection.scss";
import XeCard from "../../components/XeCard";

const LandingPageThirdSection = () => {
  return (
    <>
      <div id={`applications`} className="xc-landing-third-section">
        <div className="xc-landing-third-section-conteiner">
          <div className="xc-landing-third-section-wrapper">
            <div className="display-item-center">
              <h1>APPLICATIONS WE OFFER</h1>
            </div>
            <div className="xc-landing-card-section aline-item-space-btwn">
              <XeCard extraClass={`card-1`}>
                <img src={DataOne} alt="" />
                <span>Data One</span>
                <h1>FBA profit & loss Analysis App</h1>
                <h2>- Realtime Dashboard</h2>
                <h2>- Reports & Ananlysis</h2>
                <h2>- Inventory Status Details</h2>
                <h2>- Orde Status Details</h2>
                <h2>- Supplier Invoice Details</h2>
                <h3>
                  Know More <img src={RightArrow} alt="" />
                </h3>
              </XeCard>

              <XeCard extraClass={`card-2`}>
                <img src={BuyBox} alt="" />
                <span>BuyBox</span>

                <h1>Repricer App</h1>

                <h2>- Realtime Dashboard</h2>
                <h2>- Reports & Ananlysis</h2>
                <h2>- Competitors Analysis</h2>
                <h2>- Flexible Rules</h2>
                <h2>- Simple & Quick</h2>
                <h3>
                  Know More <img src="assets/landing/rightarrow.png" alt="" />
                </h3>
              </XeCard>
              <XeCard extraClass={`card-3`}>
                <img src={Repay} alt="" />
                <span>Repay++</span>
                <h1>Reimbursement App</h1>

                <h2>Realtime Dashboard</h2>
                <h2>Refund Guardian</h2>
                <h2>Inventory Protector</h2>
                <h2>Feedback Defender</h2>
                <h2>Custom Case</h2>
                <h3>
                  Know More <img src={RightArrow} alt="" />
                </h3>
              </XeCard>
            </div>

            <div className="xc-landing-card-section aline-item-space-btwn">
              <XeCard extraClass={`card-4`}>
                <img src={FiveStar} alt="" />
                <span>Fivestar</span>
                <h1>Feedback App</h1>

                <h2>- Realtime Dashboard</h2>
                <h2>- Campaign Management</h2>
                <h2>- Email Hub</h2>
                <h2>- Reports & Analysis</h2>
                <h3>
                  Know More <img src={RightArrow} alt="" />
                </h3>
              </XeCard>
              <XeCard extraClass={`card-5`}>
                <img src={OrderAI} alt="" />
                <span>Order AI</span>
                <h1>Order App</h1>
                <h2>- Pre-Integration with Marketplaces</h2>
                <h2>- Single Panel to Manage Orders</h2>
                <h2>- Reports and Analytic</h2>
                <h2>- Easy Invoice Generation</h2>
                <h2>- Easy Shipping Label and Manifest Creation</h2>
                <h3>
                  Know More <img src={RightArrow} alt="" />
                </h3>
              </XeCard>
              <XeCard extraClass={`inActive`}>
                <div className="coloum-center">
                  <div className="pannel-center">
                    <span>Stay Tunned</span>
                    <h2>We will launch more Applications soon!</h2>
                  </div>
                </div>
              </XeCard>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default LandingPageThirdSection;
