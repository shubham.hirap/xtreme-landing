import React from "react";
import GrowBusiness from "../../assets/assets/landing/GrowBusiness.png";
import "./landingPageSixthSection.scss";

const LandingPageSixthSection = () => {
  return (
    <>
      <div id="pricing" className="xc-landing-sixth-section">
        <div className="xc-landing-sixth-section-container">
          <div className="xc-landing-sixth-section-wrapper">
            <span>OVER ALL EXTREME ECOMMERCE PRICING PLAN</span>
          </div>
        </div>
      </div>
      <div className="grow-business-img-section">
        <div className="grow-business-img">
          <img src={GrowBusiness} alt="" />
        </div>
      </div>
    </>
  );
};

export default LandingPageSixthSection;
