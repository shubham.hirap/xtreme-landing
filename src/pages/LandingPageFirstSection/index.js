import React from "react";
import XeButton from "../../components/XeButton";
import EcomImage from "../../assets/assets/landing/e-commerce.png";
import GrowthOnSales from "../../assets/assets/blueicons/growthonsales.svg";
import CustomerReach from "../../assets/assets/blueicons/customerreach.svg";
import OperationalEfficiency from "../../assets/assets/blueicons/operationalefficiency.svg";
import ReimbursementClaims from "../../assets/assets/blueicons/reimbursementclaims.svg";
import CustomerFeedback from "../../assets/assets/blueicons/customerfeedback.svg";
import MarketExposure from "../../assets/assets/blueicons/marketexposure.svg";
import IntegratedMarketplaces from "../../assets/assets/blueicons/integratedmarketplaces.svg";
import ScrollDownButtonIcon from "../../assets/assets/icons/icon-scrolldown.svg";
import "./landingPageFirstSection.scss";
import XeBadge from "../../components/XeBadge";

const LandingPageFirstSection = () => {
  return (
    <>
      <div class="xc-landing-first-section">
        <div class="xc-main-conteiner">
          <div class="xc-main-wrapper">
            <div class="aline-item-space-btwn">
              <div class="xc-landing-left-section">
                <h1>ALL-IN-ONE ECOMMERCE GROWTH PLATFORM</h1>
                <a href="/">
                  THAT ALLOWS ANYONE TO SET UP A PROFITABLE ECOMMERCE BUSINESS
                </a>
                <p>
                  Instantly connect with multiple channels around the globe and
                  run your entire ecommerce business with automation, custom
                  workflows and smart reporting.
                </p>
                <h2>LET AI WORK FOR YOU!</h2>
                <div class="aline-item-inline">
                  <div class="register-btn">
                    <XeButton hasColor={true} label={`REGISTER`} />
                  </div>
                  <div class="request-btn">
                    <XeButton hasColor={false} label={`REQUEST A DEMO`} />
                  </div>
                </div>
              </div>
              <div class="xc-landing-right-section">
                <img src={EcomImage} alt="e-com sketch" />
              </div>
            </div>
            <div class="xc-badges-container">
              <div class="xc-badges-wrapper">
                <div class="xc-landing-badges">
                  <XeBadge
                    icon={GrowthOnSales}
                    label={`20-30%`}
                    infoText={`INCREASE IN SALES`}
                  />
                  {/* <div class="badges-part">
                    <div class="badges-content">
                      <div class="badges-img">
                        <img src={GrowthOnSales} alt="Growth on Sales Logo" />
                      </div>
                      <div class="badges-number">
                        <h1>20-30%</h1>
                      </div>
                      <div class="badges-text">
                        <span>INCREASE</span>
                        <span>IN SALES</span>
                      </div>
                    </div>
                  </div> */}

                  <XeBadge
                    icon={CustomerReach}
                    label={`300M+`}
                    infoText={`CUSTOMER REACH`}
                  />
                  <XeBadge
                    icon={OperationalEfficiency}
                    label={`100%`}
                    infoText={`OPERATIONAL EFFICIENCY THROUGH AI`}
                  />
                  <XeBadge
                    icon={ReimbursementClaims}
                    label={`80%`}
                    infoText={`REIMBURSEMENT CLAIMS`}
                  />
                  <XeBadge
                    icon={CustomerFeedback}
                    label={`30%`}
                    infoText={`INCREASE FEEDBACK CUSTOMER`}
                  />
                  <XeBadge
                    icon={MarketExposure}
                    label={`$1.2t+`}
                    infoText={`MARKET EXPOSURE`}
                  />
                  <XeBadge
                    icon={IntegratedMarketplaces}
                    label={`45+`}
                    infoText={`INTEGRATED MARKET PLACES`}
                  />
                </div>
              </div>
            </div>
            <div className="scroll-bar">
              <div className="scroll-bar-section">
                <button className="scroll-bar-btn">
                  <div className="scrollDownButtonLook ">
                    <img
                      id="scrollDownButtonIcon"
                      src={ScrollDownButtonIcon}
                      alt="icon-scrolldown"
                    />
                    <div className="scrollDownText">SCROLL DOWN</div>
                  </div>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default LandingPageFirstSection;
