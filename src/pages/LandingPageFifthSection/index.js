import React from "react";
import RedCup from "../../assets/assets/landing/redcup.png";
import Malco from "../../assets/assets/landing/malco.png";
import Bellasous from "../../assets/assets/landing/bellasous.png";
import MI from "../../assets/assets/landing/MI.png";
import VirVentures from "../../assets/assets/landing/virventures.png";
import StoreIndya from "../../assets/assets/landing/storeindya.png";

import "./landingPageFifthSection.scss";

const LandingPageFifthSection = () => {
  return (
    <>
      <div className="xc-landing-fifth-section">
        <div className="xc-landing-fifth-section-container">
          <div className="xc-landing-fifth-section-wrapper">
            <span>OUR CUSTOMERS</span>
            <div className="aline-item-space-btwn customer-img">
              <img src={RedCup} alt="" />
              <img src={Malco} alt="" />
              <img src={Bellasous} alt="" />
              <img src={MI} alt="" />
              <img src={VirVentures} alt="" />
              <img src={StoreIndya} alt="" />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default LandingPageFifthSection;
